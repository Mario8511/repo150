package com.gestion.contrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.gestion.impl.PersonaDao;
import com.gestion.model.Persona;
import com.gestion.util.Dao;

@Component
@SessionScoped
@ManagedBean
public class UsuariosBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	@Qualifier("daoPersona")
	private Dao Personadao;
	
	private List<Persona> lsPersona;
	
	private Persona persona;
	//private int p;
	
	
	public List<Persona> getLsPersona() {
		this.lsPersona = Personadao.findAll();
		return lsPersona;
	}
	public void setLsPersona(List<Persona> lsPersona) {
		this.lsPersona = lsPersona;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	@PostConstruct
	public void init() {
		persona = new Persona();
	}
	
	public void listar() {
		try {
			lsPersona = Personadao.findAll();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
