package com.gestion.util;

import java.util.List;

public interface Dao<T> {
	
	public void create(T t);
	
	public void edit(T t);
	
	public void remove(T t);
	
	public List<T> findAll();
	
	public T find(Object e);

}
