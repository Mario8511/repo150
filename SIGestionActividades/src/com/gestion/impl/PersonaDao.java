package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.Persona;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class PersonaDao extends AbstractFacade<Persona> implements Dao<Persona> {

	private SessionFactory sf;

	public PersonaDao(SessionFactory sf, Class<Persona> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
