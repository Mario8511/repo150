package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.Estado;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class EstadoDao extends AbstractFacade<Estado> implements Dao<Estado> {
	
	private SessionFactory sf;

	public EstadoDao(SessionFactory sf, Class<Estado> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
		

}
