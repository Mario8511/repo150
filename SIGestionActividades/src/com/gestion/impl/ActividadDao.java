package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.Actividad;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class ActividadDao extends AbstractFacade<Actividad> implements Dao<Actividad> {
	
	private SessionFactory sf;

	public ActividadDao(SessionFactory sf, Class<Actividad> entityClass) {
		super(entityClass);
		this.sf = sf;		
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
	
	
	

}
