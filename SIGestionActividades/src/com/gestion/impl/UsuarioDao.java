package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.Usuario;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class UsuarioDao extends AbstractFacade<Usuario> implements Dao<Usuario> {

	private SessionFactory sf;

	public UsuarioDao(SessionFactory sf, Class<Usuario> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
