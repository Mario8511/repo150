package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.FechasEspe;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class FechasEspeDao extends AbstractFacade<FechasEspe> implements Dao<FechasEspe> {
	
	private SessionFactory sf;

	public FechasEspeDao(SessionFactory sf, Class<FechasEspe> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}	
}
