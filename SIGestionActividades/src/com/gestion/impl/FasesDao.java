package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.Fases;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class FasesDao extends AbstractFacade<Fases> implements Dao<Fases>{

	private SessionFactory sf;

	public FasesDao(SessionFactory sf, Class<Fases> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
	
}
