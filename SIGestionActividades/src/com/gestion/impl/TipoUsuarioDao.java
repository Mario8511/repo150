package com.gestion.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.model.TipoUsuario;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class TipoUsuarioDao extends AbstractFacade<TipoUsuario> implements Dao<TipoUsuario> {

	private SessionFactory sf;

	public TipoUsuarioDao(SessionFactory sf, Class<TipoUsuario> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
